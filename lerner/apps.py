from django.apps import AppConfig


class LernerConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'lerner'
