from django.db import models
from django.db.models.fields import CharField


class Word(models.Model):
    text = CharField(max_length=255)
    type = models.ForeignKey('WordType', on_delete=models.DO_NOTHING, default=None, null=True)


class WordType(models.Model):
    name = CharField(max_length=40)
